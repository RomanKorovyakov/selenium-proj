package my_example;

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;		
import org.testng.annotations.Test;	
import org.testng.annotations.BeforeTest;	
import org.testng.annotations.AfterTest;

public class NewTest {		
	    private WebDriver driver;		
		@Test				
		public void testEasy() throws InterruptedException {
			driver.get("http://demo.guru99.com/test/guru99home/");  
			String title = driver.getTitle();		
			Thread.sleep(20);
			System.out.println("sleep for 20 sec!");
			Assert.assertTrue(title.contains("Demo Guru99 Page")); 		
		}	
		@BeforeTest
		public void beforeTest() {
			System.out.println("Welcome to Maven World");
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		    driver = new ChromeDriver();
		}
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}		
}	
